#--------README--------  

##Usage guide  

    1. clone this repository  
    2. download docker for your environment (docker-toolbox for windows home)  
    3. run docker-compose up -d in for example docker quickstart terminal  
    4. open localhost in web browser if you are using docker or http://192.168.99.100/ if you are using docker-toolbox  
    5. Follow instructions in the browser  
